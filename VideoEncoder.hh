/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**********/
// Choon Jin Ng
// Copyright (c) 2005 ViSLAB, University of Sydney.  All rights reserved.
// Video Encoder
// C++ header

#ifndef VIDEOENCODER_HH
#define VIDEOENCODER_HH

extern "C"{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
  #include "libswscale/swscale.h"
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
};

#define STREAM_PIX_FMT PIX_FMT_RGB565 /* pix format from VNC - PIX_FMT_RGB565 */

class VideoEncoder {
	private:
		int buffsize;
		int width, height, fps;
		char *format;
	
		//video output
		AVFrame *picture, *tmp_picture;
		AVOutputFormat *fmt;
		AVFormatContext *oc;
		AVStream *video_st;
  //frame pix convert (replacing img_convert)
  struct SwsContext *img_convert_ctx;
	
	public:
		u_int8_t *video_outbuf;
	
	private:
		bool open_video(AVFormatContext *oc, AVStream *st);
		AVFrame* alloc_picture(enum PixelFormat pix_fmt);
		AVStream* add_video_stream(AVFormatContext *oc, int codec_id);
	
	public:
		VideoEncoder(int buffsize=200000, char *format="mjpeg", int width=0, int height=0, int fps=25);
		~VideoEncoder();
		bool movie_open();
		int write_video_frame(int maxSize);
		void close_video();
		u_int8_t* getVideoInputBuffer();
		u_int8_t* getVideoOutputBuffer();
		int getBufferSize();
};

#endif

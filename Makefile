# Arch, Ubuntu...
DISTRO=Ubuntu

PREFIX=/usr/local
bindir=$(PREFIX)/bin
DESTDIR=

# i386,x86_64,etc
ARCH=i386
# needed by x264 on Deb-likes
#PKG_CONFIG_PATH:=$(PKG_CONFIG_PATH):/usr/lib/$(ARCH)-linux-gnu/pkgconfig

CC=g++
COPTS=-Wall
# random opts
ifdef DEBUG
DOPTS=-DOUTPUT_JPG_FILE
DOPTS+=-DCVNDBG
endif
# -D__STDC_CONSTANT_MACROS for the sake of libavutil/common.h
CXXFLAGS=$(COPTS) -g -D__STDC_CONSTANT_MACROS $(DOPTS)
# CXXFLAGS

PROGNAME=vncast
SOURCE=VNCastStreamer.cpp MJPEGDeviceSource.cpp VideoEncoder.cpp
HEADER=MJPEGDeviceSource.hh VideoEncoder.hh

# LIBs section
# libvncclient; 0.9.8 on Arch has 'pkgconfig/libvnc{client,server}.pc' filez
LIBVNCSERVER_INC := $(shell libvncserver-config --cflags)
LIBVNCSERVER_LIB := $(shell libvncserver-config --libs)
# only libvncclient needed; 'pkg-config --cflags,--libs libvncclient'

# ffmpeg
FFMPEG_INC := $(shell pkg-config --cflags libavutil libavcodec libavformat libswscale)
FFMPEG_LIB := $(shell pkg-config --libs libavutil libavcodec libavformat libswscale)

# liveMedia contrivance;(
ifeq ($(DISTRO),Ubuntu)
LIVE_PREFIX := /usr/include
LIVE_SUFFIX :=
LIVE_LDFLAGS :=
# sowcily
LIVE_PIC := _pic
else ifeq ($(DISTRO),Arch)
LIVE_PREFIX := /usr/lib/live
LIVE_SUFFIX := /include
LIVE_LDFLAGS := -L$(LIVE_PREFIX)/UsageEnvironment -L$(LIVE_PREFIX)/groupsock -L$(LIVE_PREFIX)/liveMedia -L$(LIVE_PREFIX)/BasicUsageEnvironment
endif

LIVE_INC := -I$(LIVE_PREFIX)/UsageEnvironment$(LIVE_SUFFIX) -I$(LIVE_PREFIX)/groupsock$(LIVE_SUFFIX) -I$(LIVE_PREFIX)/liveMedia$(LIVE_SUFFIX) -I$(LIVE_PREFIX)/BasicUsageEnvironment$(LIVE_SUFFIX)
LIVE_LIB := $(LIVE_LDFLAGS) -lgroupsock$(LIVE_PIC) -lliveMedia$(LIVE_PIC) -lBasicUsageEnvironment$(LIVE_PIC) -lUsageEnvironment$(LIVE_PIC)

# x264 (is it needed?)
X264_INC := $(shell pkg-config --cflags x264)
X264_LIB := $(shell pkg-config --libs x264)

# continuing...
CFLAGS := $(LIBVNCSERVER_INC) $(FFMPEG_INC) $(LIVE_INC) $(X264_INC)
# compile flags
LFLAGS := $(LIBVNCSERVER_LIB) $(FFMPEG_LIB) $(LIVE_LIB) $(X264_LIB)
# link flags

OBJECT=$(SOURCE:%.cpp=%.o)

# "standard" formula
%.o: %.cpp
	$(CC) $(CXXFLAGS) $(CFLAGS) -o $@ -c $<

$(PROGNAME): $(OBJECT)
	$(CC) $(COPTS) $^ $(LFLAGS) -o $@

clean:
	rm -f $(PROGNAME) $(OBJECT)

install: $(PROGNAME)
	install -m755 $(PROGNAME) $(DESTDIR)$(bindir)

uninstall:
	rm -f $(bindir)/$(PROGNAME)

# deps by hand;(
MJPEGDeviceSource.cpp: MJPEGDeviceSource.hh ; touch $@
VideoEncoder.cpp: VideoEncoder.hh ; touch $@
VNCastStreamer.cpp: MJPEGDeviceSource.hh ; touch $@
MJPEGDeviceSource.hh: VideoEncoder.hh ; touch $@
